a.) What form of inheritance is involved in the following case? Background theory: inheritance and polymorphism.

```java
abstract class CommandLineApp {
    String ask(String prompt) {
        System.out.print(prompt + ": ");
        return new java.util.Scanner(System.in).next();
    }
}

class LoginScreen extends CommandLineApp {
    void lock() {
        while(true) {
            var id = ask("Username");
            var pw = ask("Passoword");

            if (id.equals("root") && pw.equals("root"))
                return;

            try {
                Thread.sleep(1000);
            }
            catch(Exception e) {}
        }
    }
}
```

Answer: The type of inheritance seems to be Specialization, as it inherits and adds new methods. It's like common iheritance from one superclass.


b.) Comment on and evaluate the following code. Why does it work / not work? What are the benefits and drawbacks associated with it? Background theory and context of the task: inheritance and polymorphism.

```java
interface NaturalResource {
    // How much of the natural resources are left?
    // @.pre true
    // @.post RESULT == (amount let)
    float amountLeft();

    // Spend x percent
    // @.pre amount >= 0
    // @.post amountLeft() == amount + OLD(amountLeft())
    void spend(float amount);
}

class Coal implements NaturalResource {
    private float left;

    Coal(float amount) {
        left = amount;
    }

    @Override
    public float amountLeft() {
        return left;
    }

    @Override
    public void spend(float amount) {
        left -= amount;
    }
}

class Hydroelectric implements NaturalResource {
    private float left = 100;

    @Override
    public float amountLeft() {
        return left;
    }

    // @.pre cant be called
    // @.post throws Exception
    private void spend(float amount) throws Exception {
        throw new Exception("Renewable is limitless!");
    }
}
```

Answer: Code likely won't work. I see some problems with Hydroelectric class. 

First of all I think it's strange to implement intarface in case of Hydroelectric. First of all it's not logically bound to NaturalResource, so it can be confusing. Second, there is issue with method "spend". It doesn't have @Override annotation so it doesn't conform the interfaces contract. And why would we need a method, that just throws Exception and can't be called outside of the class?

The benefits...we can have an acess to class Hydroelectric trough the intarface, but why would we anyway? 


c.) Comment on and evaluate the following code. Why does it work / not work? What are the benefits and drawbacks associated with it? Background theory and context of the task: inheritance and polymorphism.

```java
class RandomGenerator {
    Object generate() {
        var random = new java.util.Random();
        return switch (random.nextInt(4)) {
            case 1 -> 1;
            case 2 -> 2;
            case 3 -> "three";
            default -> new Object();
        };
    }
}

class RandomIntegerGenerator extends RandomGenerator {
    @Override
    Integer generate() {
        return new java.util.Random().nextInt(64);
    }
}
```

Answer: 
Upd
Code won't even compile seems like. Didn't know that return type of Overriden method needs to be exactly same, as in the parent class.

Old
Code most likely won't work. Every time RandomGenerator's method generate() is called - is made a new instance of Randonclass. Maybe it's not intended logic. random.nextInt(4) will generate an Integer from 0 to 3. So if random number is 0 - there will be created now Object, but it's nonsense to me.
First thought was that case 3 will cause a problem, but it doesn't matter that it's "three" instead of 3, because its and Object too. But it's confusing and shouldn't be made this way.

Class RandomIntegerGenerator looks fine.  Depends on situation, but maybe better will be to have a one instance of Random class instead of creating a new one each time method is called."